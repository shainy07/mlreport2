#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 13:50:56 2018

"""

import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from matplotlib import pyplot as plt
from keras.optimizers import RMSprop
from keras.datasets import mnist

# reshaping and normalizing data
num_classes = 10
epochs = 20
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = x_train.reshape(60000, 784)
x_test = x_test.reshape(10000, 784)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# drawing multiple plots of accuracy on one figure
def draw(histories, eta, name, neurons):
    fig = plt.figure(figsize = (10,5))
    ax = plt.subplot(111)
    for i in range(len(histories)):
        ax.plot(np.arange(1,21,1), histories[i].history['val_acc'], label = 'neurons: '+str(neurons[i]))
    plt.title('Model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.xticks(np.arange(0, 22, 2))
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
    ax.legend(loc = 'center left', bbox_to_anchor = (1, 0.5))
    plt.savefig(name+'.eps')
    plt.savefig(name)
    plt.show()
    
# creating neural network with different layers, learning rates and activation functions
def neural_network(activation_function_list, learning_rate, batch_size, layers_list, epochs = 20):
    model = Sequential()
    model.add(Dense(layers_list[0], activation = activation_function_list[0], input_shape = (784,)))
    for i in range(1,len(activation_function_list)):
        model.add(Dense(layers_list[i], activation = activation_function_list[i]))
    model.compile(loss = 'categorical_crossentropy',
              optimizer = RMSprop(lr = learning_rate),
              metrics = ['accuracy'])
    history = model.fit(x_train, y_train,
                    batch_size = batch_size,
                    epochs = epochs,
                    verbose = 0,
                    validation_data = (x_test, y_test))
    return history

history = neural_network(['relu', 'softmax'], 0.01, 100, [30, 10], 20)